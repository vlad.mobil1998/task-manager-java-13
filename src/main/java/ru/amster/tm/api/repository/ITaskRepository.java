package ru.amster.tm.api.repository;

import ru.amster.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneById(String id);

}