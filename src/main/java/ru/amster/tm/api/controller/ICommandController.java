package ru.amster.tm.api.controller;

public interface ICommandController {

    void showHelpMsg();

    void showArgMsg();

    void showCmdMsg();

    void showInfoMsg();

    void showAboutMsg();

    void showVersionMsg();

    void errorInputCmd();

    void errorInputArg();

    void exit();

}