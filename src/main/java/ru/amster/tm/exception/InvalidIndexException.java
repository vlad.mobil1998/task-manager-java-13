package ru.amster.tm.exception;

public class InvalidIndexException extends RuntimeException {

    public InvalidIndexException(String value) {
        super("Error! This ''" + value + "'' is not number...");
    }

    public InvalidIndexException() {
        super("Error! Index incorrect...");
    }

}