package ru.amster.tm.exception.empty;

public class EmptyProjectException extends RuntimeException {

    public EmptyProjectException() {
        super("Error! Project is empty (not found)...");
    }

}