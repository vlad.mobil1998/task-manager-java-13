package ru.amster.tm.repository;

import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project findOneById(final String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new EmptyProjectException();
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        throw new EmptyProjectException();
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new EmptyProjectException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new EmptyProjectException();
        remove(project);
        return project;
    }

    @Override
    public Project removeOneById(final String id) {
        final Project project = findOneById(id);
        if (project == null) throw new EmptyProjectException();
        remove(project);
        return project;
    }

}