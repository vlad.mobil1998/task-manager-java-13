package ru.amster.tm.controller;

import ru.amster.tm.api.controller.ICommandController;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.model.Command;
import ru.amster.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showHelpMsg() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (Command command : commands) System.out.println(command);
    }

    public void showArgMsg() {
        final String[] arguments = commandService.getARGUMENTS();
        System.out.println(Arrays.toString(arguments));
    }

    public void showCmdMsg() {
        final String[] commands = commandService.getCOMMANDS();
        System.out.println(Arrays.toString(commands));
    }

    public void errorInputCmd() {
        System.out.println("ERROR! Command not late");
    }

    public void errorInputArg() {
        System.out.println("ERROR! Argument not late");
    }

    public void showInfoMsg() {
        System.out.println("[INFO]");

        final int availableProcessor = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessor);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);

    }

    public void showAboutMsg() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Amster Vladislav");
        System.out.println("E-MAIL: vlad@amster.ru");
    }

    public void showVersionMsg() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

    public void exit() {
        System.exit(0);
    }

}