package ru.amster.tm.service;

import ru.amster.tm.api.repository.ICommandRepository;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCOMMANDS() {
        return commandRepository.getCOMMANDS();
    }

    public String[] getARGUMENTS() {
        return commandRepository.getARGUMENTS();
    }

}