package ru.amster.tm.bootstrap;

import ru.amster.tm.api.controller.ICommandController;
import ru.amster.tm.api.controller.IProjectController;
import ru.amster.tm.api.controller.ITaskController;
import ru.amster.tm.api.repository.ICommandRepository;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.constant.ProgramArgConst;
import ru.amster.tm.constant.TerminalCmdConst;
import ru.amster.tm.controller.CommandController;
import ru.amster.tm.controller.ProjectController;
import ru.amster.tm.controller.TaskController;
import ru.amster.tm.repository.CommandRepository;
import ru.amster.tm.repository.ProjectRepository;
import ru.amster.tm.repository.TaskRepository;
import ru.amster.tm.service.CommandService;
import ru.amster.tm.service.ProjectService;
import ru.amster.tm.service.TaskService;
import ru.amster.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public final void run(final String[] args) {
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCmd(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseCmd(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalCmdConst.HELP:
                commandController.showHelpMsg();
                break;
            case TerminalCmdConst.ABOUT:
                commandController.showAboutMsg();
                break;
            case TerminalCmdConst.VERSION:
                commandController.showVersionMsg();
                break;
            case TerminalCmdConst.INFO:
                commandController.showInfoMsg();
                break;
            case TerminalCmdConst.ARGUMENTS:
                commandController.showArgMsg();
                break;
            case TerminalCmdConst.COMMANDS:
                commandController.showCmdMsg();
                break;
            case TerminalCmdConst.EXIT:
                commandController.exit();
                break;
            case TerminalCmdConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalCmdConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalCmdConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case TerminalCmdConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalCmdConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalCmdConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalCmdConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalCmdConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalCmdConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalCmdConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalCmdConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalCmdConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalCmdConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalCmdConst.PROJECT_CLEAR:
                projectController.clearProject();
                break;
            case TerminalCmdConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalCmdConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalCmdConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalCmdConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalCmdConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalCmdConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalCmdConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalCmdConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            default:
                commandController.errorInputCmd();
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        for (String arg : args) {
            System.out.println(" ");
            switch (arg) {
                case ProgramArgConst.HELP:
                    commandController.showHelpMsg();
                    break;
                case ProgramArgConst.ABOUT:
                    commandController.showAboutMsg();
                    break;
                case ProgramArgConst.VERSION:
                    commandController.showVersionMsg();
                    break;
                case ProgramArgConst.INFO:
                    commandController.showInfoMsg();
                    break;
                default:
                    commandController.errorInputArg();
                    break;
            }
        }
        return true;
    }

}